#ifndef SETTINGS_H
#define SETTINGS_H

#define APP_SETTINGS_FILE "settings.conf"

class CSettings
{
public:
	CSettings();

	bool Exists();
	bool Load();
	void Save();

	char m_aUsername[32];
	char m_aPassword[32];

	char m_aSSID[32+1];
	char m_aPSK[64+1];

	bool m_DHCP;
	char m_aHostname[64];
	IpAddress m_Address;
	IpAddress m_Netmask;
	IpAddress m_Gateway;

	uint8_t m_aLockCode[8];

	struct CFingerPrint
	{
		uint16_t m_FingerNum;
		char m_aLabel[32];
		uint8_t m_aDigest[SHA256_SIZE];
	};
	HashMap<uint16_t, CFingerPrint> m_FingerPrints;
};

#endif
