#ifndef FINGERPRINT_H
#define FINGERPRINT_H

// Data package identifier
enum FingerPrintIdent
{
	IDENT_COMMAND			= 0x01, // Command packet
	IDENT_DATA				= 0x02, // Data packet;
									// Data packet shall not appear alone in executing processs,
									// must follow command packet or acknowledge packet.
	IDENT_ACK				= 0x07, // Acknowledge packet
	IDENT_ENDOFDATA			= 0x08, // End of Data packet
};

enum FingerPrintCommand
{
	COMMAND_GENIMG			= 0x01, // Collect finger image
	COMMAND_IMG2TZ			= 0x02, // To generate character file from image
	COMMAND_MATCH			= 0x03, // Carry out precise matching of two templates
	COMMAND_SEARCH			= 0x04, // Search library finger
	COMMAND_REGMODEL		= 0x05, // To combine character files and generate template
	COMMAND_STORE			= 0x06, // To store template;
	COMMAND_LOADCHAR		= 0x07, // to read/load template
	COMMAND_UPCHAR			= 0x08, // to upload template
	COMMAND_DOWNCHR			= 0x09, // to download template
	COMMAND_UPIMAGE			= 0x0A, // To upload image
	COMMAND_DOWNIMAGE		= 0x0B, // To download image
	COMMAND_DELETCHAR		= 0x0C, // to delete tempates
	COMMAND_EMPTY			= 0x0D, // to empty the library
	COMMAND_SETSYSPARA		= 0x0E, // To set system Parameter
	COMMAND_READSYSPARA		= 0x0F, // To read Parameter
	COMMAND_SETPWD			= 0x12, // To set password
	COMMAND_VFYPWD			= 0x13, // To verify password
	COMMAND_GETRANDOMCODE	= 0x14, // to get random code
	COMMAND_SETADDER		= 0x15, // To set device address
	COMMAND_CONTROL			= 0x17, // Port control
	COMMAND_WRITENOTEPAD	= 0x18, // to write note pad
	COMMAND_READNOTEPAD		= 0x19, // To read note pad
	COMMAND_HISPEEDSEARCH	= 0x1B, // Search the library fastly
	COMMAND_TEMPLATENUM		= 0x1D, // To read finger template numbers
	COMMAND_READCONLIST		= 0x1F, // To read finger template index table
};

enum FingerPrintError
{
	ERROR_OK						= 0x00, // command execution complete
	ERROR_COMMUNICATION				= 0x01, // error when receiving data package
	ERROR_NOFINGER					= 0x02, // no finger on the sensor
	ERROR_READIMAGE					= 0x03, // fail to enroll the finger
	ERROR_MESSYIMAGE				= 0x06, // fail to generate character file due to the over-disorderly fingerprint image
	ERROR_FEWFEATUREPOINTS			= 0x07, // fail to generate character file due to lackness of character point or over-smallness of fingerprint image
	ERROR_NOTMATCHING				= 0x08, // finger doesn't match
	ERROR_NOTEMPLATEFOUND			= 0x09, // fail to find the matching finger
	ERROR_CHARACTERISTICSMISMATCH	= 0x0A, // fail to combine the character files
	ERROR_INVALIDPOSITION			= 0x0B, // addressing PageID is beyond the finger library
	ERROR_LOADTEMPLATE				= 0x0C, // error when reading template from library or the template is invalid
	ERROR_UPLOADTEMPLATE			= 0x0D, // error when uploading template
	ERROR_PACKETRESPONSEFAIL		= 0x0E, // Module can't receive the following data packages
	ERROR_UPLOADIMAGE				= 0x0F, // error when uploading image
	ERROR_DELETETEMPLATE			= 0x10, // fail to delete the template
	ERROR_CLEARDATABASE				= 0x11, // fail to clear finger library
	ERROR_WRONGPASSWORD				= 0x13, // wrong password
	ERROR_INVALIDIMAGE				= 0x15, // fail to generate the image for the lackness of valid primary image
	ERROR_FLASH						= 0x18, // error when writing flash
	ERROR_NODEF						= 0x19, // No definition error
	ERROR_INVALIDREGISTER			= 0x1A, // invalid register number
	ERROR_INCORRECTREGISTERCONF		= 0x1B, // incorrect configuration of register
	ERROR_INVALIDNOTEPADPAGE		= 0x1C, // wrong notepad page number
	ERROR_COMMUNICATIONPORT			= 0x1D, // fail to operate the communication port
};

enum RecvStates
{
	RECV_DONE = 0,
	RECV_WAITING = 1,
	RECV_DROP = 2
};

struct CFingerSystemParameters
{
	uint16_t statusRegister;
	uint16_t systemID;
	uint16_t storageCapacity;
	uint16_t securityLevel;
	uint32_t deviceAddress;
	uint16_t packetLength;
	uint16_t baudRate;
};

class CFingerPrint;
typedef void (CFingerPrint::*RecvCallback)(FingerPrintIdent ident, uint8_t *pData, uint16_t length);

typedef void (*VerifyPasswordCallback)(void *pUser, FingerPrintError error, const char *errorStr);
typedef void (*ReadSystemParametersCallback)(void *pUser, FingerPrintError error, const char *errorStr, CFingerSystemParameters *param);
typedef void (*ReadImageCallback)(void *pUser, FingerPrintError error, const char *errorStr);
typedef void (*ConvertImageCallback)(void *pUser, FingerPrintError error, const char *errorStr);
typedef void (*SearchTemplateCallback)(void *pUser, FingerPrintError error, const char *errorStr, int16_t position, int16_t score);
typedef void (*CompareCharacteristicsCallback)(void *pUser, FingerPrintError error, const char *errorStr, int16_t score);
typedef void (*CreateTemplateCallback)(void *pUser, FingerPrintError error, const char *errorStr);
typedef void (*DownloadCharacteristicsCallback)(void *pUser, FingerPrintError error, const char *errorStr, int8_t *pChar, uint16_t charLen);
typedef void (*LoadTemplateCallback)(void *pUser, FingerPrintError error, const char *errorStr);
typedef void (*StoreTemplateCallback)(void *pUser, FingerPrintError error, const char *errorStr, uint16_t positionNumber);
typedef void (*ReadTemplateMapCallback)(void *pUser, FingerPrintError error, const char *errorStr, uint8_t *pData, uint16_t dataLen);

class CFingerPrint
{
public:
	CFingerPrint();
	void Init(HardwareSerial &serial, uint32_t address, uint32_t password);

	static const char *ExplainFingerError(uint8_t error);

	int VerifyPassword();
	int ReadSystemParameters(uint8_t aResponse[17]);
	int DeleteTemplate(uint16_t positionStart, uint16_t count);
	int EmptyDatabase();

	int AsyncVerifyPassword(VerifyPasswordCallback fnCallback, void *pUser);
	int AsyncReadSystemParameters(ReadSystemParametersCallback fnCallback, void *pUser);
	int AsyncReadImage(ReadImageCallback fnCallback, void *pUser);
	int AsyncConvertImage(ConvertImageCallback fnCallback, void *pUser, uint8_t numCharBuffer);
	int AsyncSearchTemplate(SearchTemplateCallback fnCallback, void *pUser, uint8_t numCharBuffer, uint16_t positionStart, uint16_t numTemplates);
	int AsyncCompareCharacteristics(CompareCharacteristicsCallback fnCallback, void *pUser);
	int AsyncCreateTemplate(CreateTemplateCallback fnCallback, void *pUser);
	int AsyncDownloadCharacteristics(DownloadCharacteristicsCallback fnCallback, void *pUser, uint8_t numCharBuffer);
	int AsyncLoadTemplate(LoadTemplateCallback fnCallback, void *pUser, uint16_t positionNumber, uint8_t numCharBuffer);
	int AsyncStoreTemplate(StoreTemplateCallback fnCallback, void *pUser, uint16_t positionNumber, uint8_t numCharBuffer);
	int AsyncReadTemplateMap(ReadTemplateMapCallback fnCallback, void *pUser, uint8_t numPage);

private:
	void OnAsyncVerifyPassword(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncReadSystemParameters(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncReadImage(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncConvertImage(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncSearchTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncCompareCharacteristics(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncCreateTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncDownloadCharacteristics(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncLoadTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncStoreTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	void OnAsyncReadTemplateMap(FingerPrintIdent ident, uint8_t *pData, uint16_t length);

	void OnData(Stream &stream, char arrivedChar, unsigned short availableCharsCount);
	int Write(FingerPrintIdent ident, uint8_t *pData, uint16_t length);
	int Recv(FingerPrintIdent *pIdent, uint8_t *pData, uint16_t maxLength, const int maxTime=100);

	uint32_t m_Address;
	uint32_t m_Password;
	HardwareSerial *m_pSerial;

	uint8_t m_aRecvBuffer[2+4+1+2+256+2];
	uint16_t m_RecvIndex;

	volatile RecvStates m_RecvState;
	RecvCallback m_fnRecvCallback;
	void *m_fnUserCallback;
	void *m_pUserData;

	uint8_t m_aBuffer[1024];
	int32_t m_iBuffer;
};

#endif
