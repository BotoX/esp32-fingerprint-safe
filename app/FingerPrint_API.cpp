#include <SmingCore.h>
#include "FingerPrint.h"

int CFingerPrint::VerifyPassword()
{
	uint8_t aPayload[] = {
		COMMAND_VFYPWD,
		(uint8_t)(m_Password >> 24 & 0xFF),
		(uint8_t)(m_Password >> 16 & 0xFF),
		(uint8_t)(m_Password >> 8 & 0xFF),
		(uint8_t)(m_Password >> 0 & 0xFF),
	};

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	FingerPrintIdent ident;
	uint8_t aResponse[1];
	int ret = Recv(&ident, aResponse, sizeof(aResponse));
	m_RecvState = RECV_DROP;

	if(ret < 0)
		return ret;

	if(ident != IDENT_ACK)
		return ERROR_COMMUNICATION;

	uint8_t error = aResponse[0];
	return error;
}

int CFingerPrint::ReadSystemParameters(uint8_t aResponse[17])
{
	uint8_t aPayload[] = {
		COMMAND_READSYSPARA,
	};

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	FingerPrintIdent ident;
	int ret = Recv(&ident, aResponse, 17);
	m_RecvState = RECV_DROP;

	if(ret < 0)
		return ret;

	if(ident != IDENT_ACK)
		return ERROR_COMMUNICATION;

	uint8_t error = aResponse[0];
	return error;
}

int CFingerPrint::DeleteTemplate(uint16_t positionStart, uint16_t count)
{
	uint8_t aPayload[] = {
		COMMAND_DELETCHAR,
		(uint8_t)(positionStart >> 8 & 0xFF),
		(uint8_t)(positionStart >> 0 & 0xFF),
		(uint8_t)(count >> 8 & 0xFF),
		(uint8_t)(count >> 0 & 0xFF),
	};

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	FingerPrintIdent ident;
	uint8_t aResponse[1];
	int ret = Recv(&ident, aResponse, sizeof(aResponse));
	m_RecvState = RECV_DROP;

	if(ret < 0)
		return ret;

	if(ident != IDENT_ACK)
		return ERROR_COMMUNICATION;

	uint8_t error = aResponse[0];
	return error;
}

int CFingerPrint::EmptyDatabase()
{
	uint8_t aPayload[] = {
		COMMAND_EMPTY,
	};

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	FingerPrintIdent ident;
	uint8_t aResponse[1];
	int ret = Recv(&ident, aResponse, sizeof(aResponse));
	m_RecvState = RECV_DROP;

	if(ret < 0)
		return ret;

	if(ident != IDENT_ACK)
		return ERROR_COMMUNICATION;

	uint8_t error = aResponse[0];
	return error;
}
