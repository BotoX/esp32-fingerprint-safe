#include <SmingCore.h>
#include <ArduinoJson.h>
#include "utils.h"
#include "Settings.h"

CSettings::CSettings()
{
	strcpy(m_aUsername, "admin");
	strcpy(m_aPassword, "admin");
	strcpy(m_aSSID, "");
	strcpy(m_aPSK, "");
	m_DHCP = true;
	strcpy(m_aHostname, "safeweb");
	m_Address = IPAddress(0, 0, 0, 0);
	m_Netmask = IPAddress(0, 0, 0, 0);
	m_Gateway = IPAddress(0, 0, 0, 0);
	memset(m_aLockCode, 0, sizeof(m_aLockCode));
}

bool CSettings::Exists()
{
	return fileExist(APP_SETTINGS_FILE);
}

bool CSettings::Load()
{
	if(!Exists())
	{
		Save();
		return true;
	}

	uint32_t size = fileGetSize(APP_SETTINGS_FILE);
	char *pData = new char[size + 1];
	fileGetContent(APP_SETTINGS_FILE, pData, size + 1);

	DynamicJsonDocument doc(1024);
	if(deserializeJson(doc, pData))
	{
		delete[] pData;
		return false;
	}

	strncpy(m_aUsername, doc["username"], sizeof(m_aUsername));
	strncpy(m_aPassword, doc["password"], sizeof(m_aPassword));

	JsonObject network = doc["network"];
	strncpy(m_aSSID, network["ssid"], sizeof(m_aSSID));
	strncpy(m_aPSK, network["passphrase"], sizeof(m_aPSK));
	m_DHCP = network["dhcp"];
	strncpy(m_aHostname, network["hostname"], sizeof(m_aHostname));
	m_Address = network["address"].as<const char *>();
	m_Netmask = network["netmask"].as<const char *>();
	m_Gateway = network["gateway"].as<const char *>();

	JsonArray lockCode = doc["lock_code"];
	uint8_t tmp = min(lockCode.size(), sizeof(m_aLockCode));
	for(uint8_t i = 0; i < tmp; i++)
		m_aLockCode[i] = lockCode[i];

	JsonArray fingerprints = doc["fingerprints"];
	uint16_t sz = fingerprints.size();
	m_FingerPrints.allocate(sz);
	for(uint16_t i = 0; i < sz; i++)
	{
		JsonObject obj = fingerprints[i];
		CFingerPrint finger;

		finger.m_FingerNum = obj["num"];
		strncpy(finger.m_aLabel, obj["label"], sizeof(finger.m_aLabel));
		hex2bytes(obj["digest"].as<const char *>(), finger.m_aDigest, sizeof(finger.m_aDigest));

		m_FingerPrints[finger.m_FingerNum] = finger;
	}

	delete[] pData;
	return true;
}

void CSettings::Save()
{
	DynamicJsonDocument doc(1024);

	doc["username"] = m_aUsername;
	doc["password"] = m_aPassword;

	JsonObject network = doc.createNestedObject("network");
	network["ssid"] = m_aSSID;
	network["passphrase"] = m_aPSK;
	network["dhcp"] = m_DHCP;
	network["hostname"] = m_aHostname;
	network["address"] = m_Address.toString();
	network["netmask"] = m_Netmask.toString();
	network["gateway"] = m_Gateway.toString();

	JsonArray lockCode = doc.createNestedArray("lock_code");
	for(uint8_t i = 0; i < sizeof(m_aLockCode); i++)
		lockCode.add(m_aLockCode[i]);

	JsonArray fingerprints = doc.createNestedArray("fingerprints");
	uint16_t tmp = m_FingerPrints.count();
	for(uint16_t i = 0; i < tmp; i++)
	{
		JsonObject obj = fingerprints.createNestedObject();
		const CFingerPrint &finger = m_FingerPrints.valueAt(i);

		obj["num"] = finger.m_FingerNum;
		obj["label"] = String(finger.m_aLabel);

		char aHexDigest[SHA256_SIZE*2+1];
		bytes2hex(finger.m_aDigest, sizeof(finger.m_aDigest), aHexDigest, sizeof(aHexDigest));
		obj["digest"] = String(aHexDigest);
	}

	String docString;
	serializeJsonPretty(doc, docString);
	fileSetContent(APP_SETTINGS_FILE, docString);
}
