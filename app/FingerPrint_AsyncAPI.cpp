#include <SmingCore.h>
#include "FingerPrint.h"

int CFingerPrint::AsyncVerifyPassword(VerifyPasswordCallback fnCallback, void *pUser)
{
	uint8_t aPayload[] = {
		COMMAND_VFYPWD,
		(uint8_t)(m_Password >> 24 & 0xFF),
		(uint8_t)(m_Password >> 16 & 0xFF),
		(uint8_t)(m_Password >> 8 & 0xFF),
		(uint8_t)(m_Password >> 0 & 0xFF),
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncVerifyPassword;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncVerifyPassword(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((VerifyPasswordCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!");
		return;
	}

	if(length != 1)
	{
		((VerifyPasswordCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!");
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	((VerifyPasswordCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr);
}


int CFingerPrint::AsyncReadSystemParameters(ReadSystemParametersCallback fnCallback, void *pUser)
{
	uint8_t aPayload[] = {
		COMMAND_READSYSPARA,
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncReadSystemParameters;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncReadSystemParameters(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((ReadSystemParametersCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!", NULL);
		return;
	}

	if(length != 1 + sizeof(CFingerSystemParameters))
	{
		((ReadSystemParametersCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!", NULL);
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	CFingerSystemParameters *param = (CFingerSystemParameters *)m_aBuffer;
	param->statusRegister     = pData[1] << 8 | pData[2];
	param->systemID           = pData[3] << 8 | pData[4];
	param->storageCapacity    = pData[5] << 8 | pData[6];
	param->securityLevel      = pData[7] << 8 | pData[8];
	param->deviceAddress      = pData[9] << 24 | pData[10] << 16 | pData[11] << 8 | pData[12];
	param->packetLength       = pData[13] << 8 | pData[14];
	param->baudRate           = pData[15] << 8 | pData[16];

	((ReadSystemParametersCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, param);
}


int CFingerPrint::AsyncReadImage(ReadImageCallback fnCallback, void *pUser)
{
	uint8_t aPayload[] = {
		COMMAND_GENIMG,
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncReadImage;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncReadImage(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((ReadImageCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!");
		return;
	}

	if(length != 1)
	{
		((ReadImageCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!");
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	((ReadImageCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr);
}


int CFingerPrint::AsyncConvertImage(ConvertImageCallback fnCallback, void *pUser, uint8_t numCharBuffer)
{
	if(numCharBuffer != 0x01 && numCharBuffer != 0x02)
		return -1;

	uint8_t aPayload[] = {
		COMMAND_IMG2TZ,
		numCharBuffer,
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncConvertImage;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncConvertImage(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((ConvertImageCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!");
		return;
	}

	if(length != 1)
	{
		((ConvertImageCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!");
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	((ConvertImageCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr);
}


int CFingerPrint::AsyncSearchTemplate(SearchTemplateCallback fnCallback, void *pUser, uint8_t numCharBuffer, uint16_t positionStart, uint16_t numTemplates)
{
	if(numCharBuffer != 0x01 && numCharBuffer != 0x02)
		return -1;

	uint8_t aPayload[] = {
		COMMAND_SEARCH,
		numCharBuffer,
		(uint8_t)(positionStart >> 8 & 0xFF),
		(uint8_t)(positionStart >> 0 & 0xFF),
		(uint8_t)(numTemplates >> 8 & 0xFF),
		(uint8_t)(numTemplates >> 0 & 0xFF),
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncSearchTemplate;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncSearchTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	int16_t position = -1;
	int16_t score = -1;

	if(ident != IDENT_ACK)
	{
		((SearchTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!", position, score);
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	if(error == ERROR_OK)
	{
		if(length != 5)
		{
			((SearchTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!", position, score);
			return;
		}

		position = pData[1] << 8 | pData[2];
		score = pData[3] << 8 | pData[4];
	}

	((SearchTemplateCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, position, score);
}


int CFingerPrint::AsyncCompareCharacteristics(CompareCharacteristicsCallback fnCallback, void *pUser)
{
	uint8_t aPayload[] = {
		COMMAND_MATCH,
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncCompareCharacteristics;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncCompareCharacteristics(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	int16_t score = -1;

	if(ident != IDENT_ACK)
	{
		((CompareCharacteristicsCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!", score);
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	if(error == ERROR_OK)
	{
		if(length != 3)
		{
			((CompareCharacteristicsCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!", score);
			return;
		}

		score = pData[1] << 8 | pData[2];
	}

	((CompareCharacteristicsCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, score);
}


int CFingerPrint::AsyncCreateTemplate(CreateTemplateCallback fnCallback, void *pUser)
{
	uint8_t aPayload[] = {
		COMMAND_REGMODEL,
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncCreateTemplate;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncCreateTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((CreateTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!");
		return;
	}

	if(length != 1)
	{
		((CreateTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!");
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	((CreateTemplateCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr);
}


int CFingerPrint::AsyncDownloadCharacteristics(DownloadCharacteristicsCallback fnCallback, void *pUser, uint8_t numCharBuffer)
{
	if(numCharBuffer != 0x01 && numCharBuffer != 0x02)
		return -1;

	uint8_t aPayload[] = {
		COMMAND_UPCHAR,
		numCharBuffer,
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncDownloadCharacteristics;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;
	m_iBuffer = -1;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncDownloadCharacteristics(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	if(m_iBuffer == -1)
	{
		if(ident != IDENT_ACK)
		{
			m_RecvState = RECV_DROP;
			((DownloadCharacteristicsCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!", NULL, 0);
			return;
		}

		uint8_t error = pData[0];
		const char *errorStr = ExplainFingerError(error);

		if(error != ERROR_OK)
		{
			m_RecvState = RECV_DROP;
			((DownloadCharacteristicsCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, NULL, 0);
			return;
		}

		m_iBuffer = 0;
		m_RecvState = RECV_WAITING;
		return;
	}

	if(ident != IDENT_DATA && ident != IDENT_ENDOFDATA)
	{
		m_RecvState = RECV_DROP;
		((DownloadCharacteristicsCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no data packet", NULL, 0);
		return;
	}

	if(sizeof(m_aBuffer) - m_iBuffer < length)
	{
		m_RecvState = RECV_DROP;
		((DownloadCharacteristicsCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Not enough space to store received data", NULL, 0);
		return;
	}

	memcpy(&m_aBuffer[m_iBuffer], pData, length);
	m_iBuffer += length;

	if(ident == IDENT_ENDOFDATA)
	{
		m_RecvState = RECV_DROP;
		((DownloadCharacteristicsCallback)m_fnUserCallback)(m_pUserData, ERROR_OK, ExplainFingerError(ERROR_OK), (int8_t *)m_aBuffer, m_iBuffer);
		return;
	}

	m_RecvState = RECV_WAITING;
}


int CFingerPrint::AsyncLoadTemplate(LoadTemplateCallback fnCallback, void *pUser, uint16_t positionNumber, uint8_t numCharBuffer)
{
	if(numCharBuffer != 0x01 && numCharBuffer != 0x02)
		return -1;

	uint8_t aPayload[] = {
		COMMAND_LOADCHAR,
		numCharBuffer,
		(uint8_t)(positionNumber >> 8 & 0xFF),
		(uint8_t)(positionNumber >> 0 & 0xFF),
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncLoadTemplate;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncLoadTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((LoadTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!");
		return;
	}

	if(length != 1)
	{
		((LoadTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!");
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	((LoadTemplateCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr);
}


int CFingerPrint::AsyncStoreTemplate(StoreTemplateCallback fnCallback, void *pUser, uint16_t positionNumber, uint8_t numCharBuffer)
{
	if(numCharBuffer != 0x01 && numCharBuffer != 0x02)
		return -1;

	uint8_t aPayload[] = {
		COMMAND_STORE,
		numCharBuffer,
		(uint8_t)(positionNumber >> 8 & 0xFF),
		(uint8_t)(positionNumber >> 0 & 0xFF),
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncStoreTemplate;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;
	m_iBuffer = positionNumber;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncStoreTemplate(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((StoreTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!", m_iBuffer);
		return;
	}

	if(length != 1)
	{
		((StoreTemplateCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "Incorrect data length!", m_iBuffer);
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	((StoreTemplateCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, m_iBuffer);
}


int CFingerPrint::AsyncReadTemplateMap(ReadTemplateMapCallback fnCallback, void *pUser, uint8_t numPage)
{
	uint8_t aPayload[] = {
		COMMAND_READCONLIST,
		numPage
	};

	m_fnRecvCallback = &CFingerPrint::OnAsyncReadTemplateMap;
	m_fnUserCallback = (void *)fnCallback;
	m_pUserData = pUser;

	Write(IDENT_COMMAND, aPayload, sizeof(aPayload));

	return 0;
}

void CFingerPrint::OnAsyncReadTemplateMap(FingerPrintIdent ident, uint8_t *pData, uint16_t length)
{
	m_RecvState = RECV_DROP;

	if(ident != IDENT_ACK)
	{
		((ReadTemplateMapCallback)m_fnUserCallback)(m_pUserData, ERROR_COMMUNICATION, "The received packet is no ack packet!", NULL, 0);
		return;
	}

	uint8_t error = pData[0];
	const char *errorStr = ExplainFingerError(error);

	if(error != ERROR_OK)
	{
		((ReadTemplateMapCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, NULL, 0);
		return;
	}

	((ReadTemplateMapCallback)m_fnUserCallback)(m_pUserData, (FingerPrintError)error, errorStr, &pData[1], length - 1);
}
