#ifndef MAIN_H
#define MAIN_H

#define FINGER_DETECT_PIN 4
#define FINGER_ENABLE_PIN 5

#define SAFELOCK_DATA_PIN 12
#define SAFELOCK_DETECT_PIN 14

#define DOOR_DETECT_PIN 13

#include <ArduinoJson.h>
#include "Settings.h"
#include "FingerPrint.h"
#include "FingerLogic.h"

class CMain
{
public:
	CMain();
	void Init(HardwareSerial &serial);

	void OnFingerInterrupt(bool finger);

	void FingerEnable(bool enable);

	void LockUnlock();
	void EnrollMessage(const char *msg, bool error=false);

	void OnFingerVerified(uint16_t fingerNum, uint8_t digest[SHA256_SIZE]);
	void OnFingerEnrolled(uint16_t fingerNum, uint8_t digest[SHA256_SIZE]);

	CSettings &Settings() { return m_Settings; }
	CFingerPrint &FingerPrint() { return m_FingerPrint; }
	CFingerLogic &FingerLogic() { return m_FingerLogic; }

private:
	bool HttpAuthorized(HttpRequest &request, HttpResponse &response);
	void HttpOnApi(HttpRequest &request, HttpResponse &response);
	void HttpOnFile(HttpRequest &request, HttpResponse &response);

	HttpStatus HandleApi(HttpMethod method, String endpoint, JsonDocument &req, JsonDocument &resp);

private:
	void LockSendBytes(uint8_t *pBytes, uint8_t len);
	void LockSendCode(uint8_t code[8]);

	CSettings m_Settings;
	CFingerPrint m_FingerPrint;
	CFingerLogic m_FingerLogic;

	FtpServer m_FTP;
	HttpServer m_HttpServer;

	String m_EnrollMessage;
	String m_EnrollLabel;
	CSettings::CFingerPrint m_EnrolledFinger;
	bool m_Enrolling;
	bool m_Enrolled;
};

extern CMain g_Main;
extern HardwareSerial Serial1;

#endif
