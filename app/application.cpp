#include <SmingCore.h>
#include "FingerPrint.h"
#include "main.h"

HardwareSerial Serial1(UART_ID_1);
NtpClient ntpClient("at.pool.ntp.org", 3600);

void IRAM_ATTR OnFingerInterrupt()
{
	// LOW = FINGER, HIGH = NO FINGER
	bool status = digitalRead(FINGER_DETECT_PIN);

	g_Main.OnFingerInterrupt(!status);
}

void ready()
{
	debugf("READY!");

	gpio_pin_wakeup_enable(GPIO_ID_PIN(FINGER_DETECT_PIN), GPIO_PIN_INTR_LOLEVEL);

	g_Main.Init(Serial);

	attachInterrupt(FINGER_DETECT_PIN, OnFingerInterrupt, CHANGE);
}

void init()
{
	// for fingerprint module
	Serial.systemDebugOutput(false);
	Serial.begin(115200, SERIAL_8N1, SERIAL_FULL);
	Serial.flush(); // TODO: Full flush?

	// for debug messages
	Serial1.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);
	Serial1.systemDebugOutput(true);

	// p-channel FET to turn on fingerprint module
	pinMode(FINGER_ENABLE_PIN, OUTPUT_OPEN_DRAIN);
	digitalWrite(FINGER_ENABLE_PIN, 1);

	// communication with safe lock
	pinMode(SAFELOCK_DATA_PIN, OUTPUT_OPEN_DRAIN);
	digitalWrite(SAFELOCK_DATA_PIN, 1);

	pinMode(FINGER_DETECT_PIN, INPUT);
	pinMode(SAFELOCK_DETECT_PIN, INPUT);
	pinMode(DOOR_DETECT_PIN, INPUT);

	// mount spiffs
	spiffs_mount();

	System.onReady(ready);
}
