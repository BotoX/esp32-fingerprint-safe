#ifndef UTILS_H
#define UTILS_H

int hex2bytes(const char *str, uint8_t *bytes, int32_t length);
void bytes2hex(const uint8_t *bytes, int32_t length, char *str, int32_t strLength);
int Rssi2Quality(sint8 rssi);

#endif
