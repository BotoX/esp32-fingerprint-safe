#ifndef FINGERLOGIC_H
#define FINGERLOGIC_H

#include <bitset>
#include "FingerPrint.h"

enum FingerLogicState
{
	STATE_ERROR = -1,
	STATE_INITIAL = 0,

	STATE_INIT_VERIFYPASSWORD,
	STATE_INIT_READSYSTEMPARAMETERS,
	STATE_INIT_READTEMPLATEMAP,
	STATE_INIT_VERIFYTEMPLATES,

	STATE_READY,

	STATE_VERIFY_READIMAGE,
	STATE_VERIFY_CONVERTIMAGE,
	STATE_VERIFY_SEARCHTEMPLATE,
	STATE_VERIFY_LOADTEMPLATE,
	STATE_VERIFY_DOWNLOADCHARACTERISTICS,

	STATE_ENROLL_WAITFINGER1,
	STATE_ENROLL_READIMAGE1,
	STATE_ENROLL_CONVERTIMAGE1,
	STATE_ENROLL_SEARCHTEMPLATE,
	STATE_ENROLL_WAITFINGER2,
	STATE_ENROLL_READIMAGE2,
	STATE_ENROLL_CONVERTIMAGE2,
	STATE_ENROLL_COMPARE,
	STATE_ENROLL_CREATETEMPLATE,
	STATE_ENROLL_STORETEMPLATE,
	STATE_ENROLL_LOADTEMPLATE,
	STATE_ENROLL_DOWNLOADCHARACTERISTICS
};

class CFingerLogic
{
public:
	CFingerLogic(class CMain *pMain);
	void Init(CFingerPrint *pFingerPrint);
	void OnFingerInterrupt(bool finger);

	bool FingerSlot(uint16_t index);
	bool FingerSlot(uint16_t index, bool value);
	int FirstFreeFingerSlot();

	void InitFinger();
	static void InitFinger_OnVerifyPassword(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void InitFinger_OnReadSystemParameters(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, CFingerSystemParameters *param);
	static void InitFinger_OnGetTemplates(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void InitFinger_OnReadTemplateMap(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, uint8_t *pData, uint16_t dataLen);
	void InitFinger_VerifyTemplates();
	static void InitFinger_OnLoadTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void InitFinger_OnDownloadCharacteristics(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, int8_t *pChar, uint16_t charLen);

	void VerifyFinger();
	static void VerifyFinger_OnReadImage(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void VerifyFinger_OnConvertImage(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void VerifyFinger_OnSearchTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, int16_t position, int16_t score);
	static void VerifyFinger_OnLoadTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void VerifyFinger_OnDownloadCharacteristics(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, int8_t *pChar, uint16_t charLen);

	bool EnrollFinger(bool cancel=false);
	static void EnrollFinger_OnReadImage1(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void EnrollFinger_OnConvertImage1(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void EnrollFinger_OnSearchTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, int16_t position, int16_t score);
	void EnrollFinger_OnFinger();
	static void EnrollFinger_OnReadImage2(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void EnrollFinger_OnConvertImage2(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void EnrollFinger_OnCompareCharacteristics(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, int16_t score);
	static void EnrollFinger_OnCreateTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void EnrollFinger_OnStoreTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, uint16_t positionNumber);
	static void EnrollFinger_OnLoadTemplate(CFingerLogic *pThis, FingerPrintError error, const char *errorStr);
	static void EnrollFinger_OnDownloadCharacteristics(CFingerLogic *pThis, FingerPrintError error, const char *errorStr, int8_t *pChar, uint16_t charLen);

	CMain &Main() { return *m_pMain; }
	CFingerPrint &FingerPrint() { return *m_pFingerPrint; }

private:
	bool m_Power;
	void PowerOff();
	void PowerOn();
	void SetState(FingerLogicState state);
	Timer m_PowerOffTimer;

	CMain *m_pMain;
	CFingerPrint *m_pFingerPrint;
	FingerLogicState m_State;
	bool m_Finger;

	CFingerSystemParameters m_FingerParams;
	uint8_t m_aFingerSlots[1792/8];
	uint16_t m_FingerSlotsAdded;

	int32_t m_iBuffer;

	Timer m_Timer;
	void *m_fnUserCallback;
	void *m_pUserData;
};

#endif
